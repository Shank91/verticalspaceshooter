﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace SpaceShooter
{
    /// <summary>
    /// User interface manager.
    /// The class repsonsible for opening/closing panels based on events.
    /// It maintains a list of all the panels in the game.
    /// </summary>
    public class UIManager : MonoBehaviour
    {
        public List<BaseUIPanel> uiPanels;

        void OnEnable()
        {
            Messenger<UIPanels>.AddListener(GameEvents.PanelOpen, OnPanelOpen);
            Messenger<int>.AddListener(GameEvents.StartGame, OnStartGame);
            Messenger.AddListener(GameEvents.Menu, OnMenu);
            Messenger.AddListener(GameEvents.GameOver, OnGameOver);
            Messenger.AddListener(GameEvents.ShowHighScores, OnShowHighScores);
            Messenger<int, string>.AddListener(GameEvents.GameWin, OnGameWin);
            Messenger.AddListener(GameEvents.ResumeGame, OnResumeGame);
        }

        void OnDisable()
        {
            Messenger<UIPanels>.RemoveListener(GameEvents.PanelOpen, OnPanelOpen);
            Messenger<int>.RemoveListener(GameEvents.StartGame, OnStartGame);
            Messenger.RemoveListener(GameEvents.Menu, OnMenu);
            Messenger.RemoveListener(GameEvents.GameOver, OnGameOver);
            Messenger.RemoveListener(GameEvents.ShowHighScores, OnShowHighScores);
            Messenger<int, string>.RemoveListener(GameEvents.GameWin, OnGameWin);
            Messenger.RemoveListener(GameEvents.ResumeGame, OnResumeGame);
        }

        void OnPanelOpen(UIPanels panel)
        {
            foreach(var p in uiPanels)
            {
                if(p.uiPanel != panel)
                {
                    p.ClosePanel();
                }
            }
        }

        void OnResumeGame()
        {
            OpenPanel(UIPanels.GameHUD);
        }

        void OnGameWin(int multiplier, string message)
        {
            OpenPanel(UIPanels.GameWin, new object[]{ message });
        }

        void OnStartGame(int playerCount)
        {
            OpenPanel(UIPanels.GameHUD);
        }

        void OnMenu()
        {
            OpenPanel(UIPanels.GameStart);
        }

        void OnGameOver()
        {
            OpenPanel(UIPanels.GameOver);
        }

        void OnShowHighScores()
        {
            OpenPanel(UIPanels.HighScores);
        }

        void OpenPanel(UIPanels panelType, object[] parameters = null)
        {
            var panel = uiPanels.Single(x => x.uiPanel == panelType);
            if(panel != null)
            {
                panel.OpenPanel(parameters);
            }
        }
    }
}