﻿using TMPro;
using UnityEngine;

namespace SpaceShooter
{
    /// <summary>
    /// Game window panel.
    /// </summary>
    public class GameWinPanel : BaseUIPanel
    {
        public TextMeshProUGUI headingText;

        public override void OpenPanel(object[] parameters = null)
        {
            base.OpenPanel(parameters);
            string heading = string.Empty;
            try
            {
                heading = parameters[0].ToString();
            }
            catch(System.Exception e)
            {
                Debug.Log(e);
            }

            headingText.SetText(heading);
        }

        public void OnContinue()
        {
            Messenger.Invoke(GameEvents.ResumeGame);
        }
    }
}
