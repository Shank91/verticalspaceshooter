﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SpaceShooter
{
    /// <summary>
    /// Game over panel.
    /// </summary>
    public class GameOverPanel : BaseUIPanel
    {
        public void OnPlayAgain()
        {
            Messenger.Invoke(GameEvents.PlayAgain);
        }

        public void OnShowHighScores()
        {
            Messenger.Invoke(GameEvents.ShowHighScores);
        }
    }
}