﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace SpaceShooter
{
    /// <summary>
    /// High score UI.
    /// </summary>
    public class HighScoreUI : MonoBehaviour
    {
        public TextMeshProUGUI rankText;
        public TextMeshProUGUI scoreText;

        public void Initialize(string rank, string score)
        {
            rankText.SetText(rank);
            scoreText.SetText(score);
        }
    }
}
