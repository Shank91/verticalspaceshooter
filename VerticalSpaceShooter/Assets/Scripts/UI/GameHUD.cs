﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace SpaceShooter
{
    /// <summary>
    /// Game HUD.
    /// Inherits from BaseUIPanel.
    /// Responsible for showing the updated score, lives of the players.
    /// Responsible for showing the current high score.
    /// </summary>
    public class GameHUD : BaseUIPanel
    {
        public TextMeshProUGUI highScoreText;
        Dictionary<int,PlayerHUD> playerhuds;

        void Awake()
        {
            Messenger<int,PlayerHUD>.AddListener(GameEvents.SetupPlayerHUD, OnSetupPlayerHUD);
            Messenger<int,int>.AddListener(GameEvents.UpdateLives, OnUpdateLives);
            Messenger<int,int>.AddListener(GameEvents.UpdateScore, OnUpdateScore);
            Messenger<int>.AddListener(GameEvents.ShowHighestScore, OnShowHighestScore);
            Messenger<int>.AddListener(GameEvents.PlayerGameOver, OnPlayerGameOver);
        }

        void OnDestroy()
        {
            Messenger<int,PlayerHUD>.RemoveListener(GameEvents.SetupPlayerHUD, OnSetupPlayerHUD);
            Messenger<int,int>.RemoveListener(GameEvents.UpdateLives, OnUpdateLives);
            Messenger<int,int>.RemoveListener(GameEvents.UpdateScore, OnUpdateScore);
            Messenger<int>.RemoveListener(GameEvents.ShowHighestScore, OnShowHighestScore);
            Messenger<int>.RemoveListener(GameEvents.PlayerGameOver, OnPlayerGameOver);
        }

        void Start()
        {
            playerhuds = new Dictionary<int,PlayerHUD>();
        }

        void OnSetupPlayerHUD(int playerId, PlayerHUD hudPrefab)
        {
            PlayerHUD newHUD = Instantiate(hudPrefab, transform);
            playerhuds.Add(playerId, newHUD);
        }

        void OnUpdateScore(int playerId, int score)
        {
            playerhuds[playerId].scoreText.SetText("Score: {0}", score);
        }

        void OnUpdateLives(int playerId, int lives)
        {
            playerhuds[playerId].livesText.SetText("Lives: {0}", lives);
        }

        void OnPlayerGameOver(int playerId)
        {
            playerhuds[playerId].livesText.SetText("Game Over");
        }

        void OnShowHighestScore(int highestScore)
        {
            highScoreText.SetText("High Score: {0}", highestScore);
        }
    }
}
