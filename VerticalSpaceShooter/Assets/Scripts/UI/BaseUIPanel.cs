﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SpaceShooter
{
    /// <summary>
    /// Base user interface panel class.
    /// Contains methods for opening and closing panel.
    /// </summary>
    public abstract class BaseUIPanel : MonoBehaviour
    {
        public UIPanels uiPanel;

        public virtual void OpenPanel(object[] parameters = null)
        {
            gameObject.SetActive(true);
            Messenger<UIPanels>.Invoke(GameEvents.PanelOpen, uiPanel);
        }

        public virtual void ClosePanel()
        {
            gameObject.SetActive(false);
        }
    }

    public enum UIPanels
    {
        Unassigned = 0,
        GameStart = 1,
        GameOver = 2,
        HighScores = 3,
        GameHUD = 4,
        GameWin = 5
    }
}

