﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SpaceShooter
{
    /// <summary>
    /// Game start panel.
    /// </summary>
    public class GameStartPanel : BaseUIPanel
    {
        public void OnOnePlayerGame()
        {
            Messenger<int>.Invoke(GameEvents.StartGame, 1);
        }

        public void OnTwoPlayerGame()
        {
            Messenger<int>.Invoke(GameEvents.StartGame, 2);
        }
    }
}