﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace SpaceShooter
{
    /// <summary>
    /// High score panel.
    /// </summary>
    public class HighScorePanel : BaseUIPanel
    {
        public HighScoreUI uiPrefab;
        public RectTransform content;
        const string HighScoreSave = "HighScoreSave";

        public override void OpenPanel(object[] parameters = null)
        {
            base.OpenPanel(parameters);
            DisplayHighScores();
        }

        void DisplayHighScores()
        {
            string json = PlayerPrefs.GetString(HighScoreSave);
            HighScores highScores = JsonUtility.FromJson<HighScores>(json);
            if(highScores == null||highScores.scores == null)
            {
                return;
            }

            List<int> scores = highScores.scores.OrderByDescending(x => x).ToList();
            for(int i=0; i<scores.Count; i++)
            {
                HighScoreUI scoreUI = Instantiate<HighScoreUI>(uiPrefab, content);
                scoreUI.Initialize((i+1).ToString(), scores[i].ToString());
            }
        }

        public void OnPlayAgain()
        {
            Messenger.Invoke(GameEvents.PlayAgain);
        }
    }
}
