﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace SpaceShooter
{
    /// <summary>
    /// Pooled object.
    /// Base abstract class for all pooled entities.
    /// There are methods for initializing, updating and disposing the entity.
    /// </summary>
    [RequireComponent(typeof(Rigidbody2D), typeof(BoxCollider2D))]
    public abstract class PooledObject : MonoBehaviour, IPooledObject
    {
        public SpriteRenderer renderer;
        protected BoxCollider2D Collider{ get; private set; }
        protected Rigidbody2D Rigidbody{ get; private set; }
        protected IObjectInfo Info{ get; private set; }
        protected Bounds LifeZoneBounds{ get; private set; }
        protected Action OnDispose{ get; private set; }

        void Awake()
        {
            Collider = GetComponent<BoxCollider2D>();
            Rigidbody = GetComponent<Rigidbody2D>();
        }

        /// <summary>
        /// Initializes the pooled object entity.
        /// Extracts the needed data.
        /// Sets the entity sprite.
        /// Updates the collider size based on the renderer bounds.
        /// Adds additional info based on how the object is getting initialized.
        /// Defines the bounds for the entity.
        /// Adds a callback for dispose.
        /// </summary>
        /// <param name="data">Data.</param>
        /// <param name="sprite">Sprite.</param>
        /// <param name="info">Info.</param>
        /// <param name="lifeZoneBounds">The bounds within which the bullet can exist.</param>
        /// <param name="onDispose">On dispose.</param>
        public virtual void Initialize(IPooledObjectData data, Sprite sprite, IObjectInfo info, Bounds lifeZoneBounds, Action onDispose)
        {
            ExtractData(data);
            Info = info;
            LifeZoneBounds = lifeZoneBounds;
            OnDispose = onDispose;

            renderer.sprite = sprite;
            Collider.size = renderer.bounds.size;

            InitializeTransform();
            gameObject.SetActive(true);
        }

        /// <summary>
        /// Updates the entity's position.
        /// If the entity goes out of playable bounds, disposes off the entity.
        /// </summary>
        public virtual void OnUpdate()
        {
            var position = (Vector2)transform.position;
            position += GetDistanceDelta() * Time.deltaTime;
            Rigidbody.MovePosition(position);
            if(!LifeZoneBounds.Contains(position))
            {
                Dispose();
            }
        }

        public virtual void Dispose()
        {
            gameObject.SetActive(false);
            OnDispose();
        }

        /// <summary>
        /// Returns the damage on direct collision with this entity.
        /// </summary>
        /// <returns>The collision damage.</returns>
        public virtual int GetCollisionDamage()
        {
            return Info.Damage;
        }

        /// <summary>
        /// Gets the id of the source/owner of this entity, if any.
        /// </summary>
        /// <returns>The source identifier.</returns>
        public virtual int GetSourceId()
        {
            return Info.SourceId;
        }

        /// <summary>
        /// Disposes of the entity when it's killed in action.
        /// </summary>
        /// <param name="killerId">Killer identifier.</param>
        public virtual void OnKilled(int killerId = -1)
        {
            Dispose();
        }
            
        protected abstract void ExtractData(IPooledObjectData data);
        protected abstract void InitializeTransform();
        protected abstract Vector2 GetDistanceDelta();
    }
}

