﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SpaceShooter
{
    /// <summary>
    /// Pooled object generator.
    /// Base abstract class for all pooled object generators.
    /// Responsible for creating a pool of elements of Component type.
    /// An inactive element can be retrieved from the pool anytime.
    /// </summary>
    public abstract class PooledObjectGenerator<T> : MonoBehaviour where T:Component
    {
        public T elementPrefab;
        public int poolCount = 5;
        List<T> pooledElements;

        void Start()
        {
            CreateElementsPool();
        }

        void CreateElementsPool()
        {
            pooledElements = new List<T>();
            for (int i = 0; i < poolCount; i++)
            {
                pooledElements.Add(NewElement());
            }
        }

        T NewElement()
        {
            T element = Instantiate<T>(elementPrefab, transform);
            element.gameObject.SetActive(false);
            return element;
        }

        /// <summary>
        /// Gets an inactive element from the pool.
        /// If the pool has run out of inactive elements, then creates a new element and returns it.
        /// </summary>
        /// <returns>The element from pool.</returns>
        public T GetElementFromPool()
        {
            if (pooledElements == null || pooledElements.Count == 0)
            {
                CreateElementsPool();
            }

            T element = pooledElements.Find(x => !x.gameObject.activeSelf);
            if (element == null)
            {
                element = NewElement();
                pooledElements.Add(element);
            }

            return element;
        }
    }
}

