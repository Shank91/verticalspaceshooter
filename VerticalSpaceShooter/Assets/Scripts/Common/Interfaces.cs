﻿using UnityEngine;

namespace SpaceShooter
{
    /// <summary>
    /// Interface for any pooled objects.
    /// </summary>
    public interface IPooledObject
    {
        void OnUpdate();

        void Dispose();
    }

    /// <summary>
    /// Interface for pooled object data.
    /// </summary>
    public interface IPooledObjectData
    {
        
    }

    /// <summary>
    /// Interface for maintaining additional info for any object.
    /// </summary>
    public interface IObjectInfo
    {
        Vector2 Position{ get; }

        float Speed{ get; }

        int Damage{ get; }

        int SourceId { get; }
    }

    /// <summary>
    /// Interface for any enemy patterns
    /// </summary>
    public interface IEnemyPattern
    {
        int Id{ get; }

        Sprite Sprite{ get; }

        EnemyData Data{ get; }
    }

    /// <summary>
    /// The interface to be used by all controllers.
    /// </summary>
    public interface IController
    {
        bool IsShooting();
        float HorizontalMovement();
        float VerticalMovement();
    }

    /// <summary>
    /// A container class for IController interface which finds all the controllers in the project for assigning in the inspector.
    /// </summary>
    [System.Serializable]
    public class Controller : IUnifiedContainer<IController> { }
}