﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SpaceShooter
{
    /// <summary>
    /// Object info.
    /// Base abstract class for any entity specific info class.
    /// Maintains additional info for any object, which isn't part of their data.
    /// </summary>
    public abstract class ObjectInfo : IObjectInfo
    {
        public Vector2 Position{ get; private set;}
        public float Speed{ get; private set; }
        public int Damage{ get; private set; }
        public int SourceId{ get; private set; }

        public ObjectInfo(Vector2 position, float speed, int damage, int sourceId = -1)
        {
            Position = position;
            Speed = speed;
            Damage = damage;
            SourceId = sourceId;
        }
    }
}
