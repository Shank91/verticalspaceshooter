﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace SpaceShooter
{
    /// <summary>
    /// Game manager.
    /// Responsible for filling up dependencies of most systems.
    /// The main MonoBehaviour class for Updating, FixedUpdating.
    /// Handles various game related events such as game start, game win, resume game and play again.
    /// </summary>
    public class GameManager : MonoBehaviour
    {
        public GameConfig gameConfig;
        public BoxCollider2D lifeZone;
        public BoxCollider2D playArea;
        public BulletManager bulletManager;
        public PlayerManager playerManager;
        public WaveSystem waveSystem;
        public GameConditions gameConditions;

        void OnEnable()
        {
            Messenger<int,string>.AddListener(GameEvents.GameWin, OnGameWin);
            Messenger.AddListener(GameEvents.ResumeGame, OnResumeGame);
            Messenger<int>.AddListener(GameEvents.StartGame, OnStartGame);
            Messenger.AddListener(GameEvents.PlayAgain, OnPlayAgain);
        }

        void OnDisable()
        {
            Messenger<int, string>.RemoveListener(GameEvents.GameWin, OnGameWin);
            Messenger.RemoveListener(GameEvents.ResumeGame, OnResumeGame);
            Messenger<int>.RemoveListener(GameEvents.StartGame, OnStartGame);
            Messenger.RemoveListener(GameEvents.PlayAgain, OnPlayAgain);
        }

        void Start()
        {
            Messenger.Invoke(GameEvents.Menu);
            bulletManager.Initialize(lifeZone);
        }

        /// <summary>
        /// Unpauses everything.
        /// </summary>
        void OnResumeGame()
        {
            Time.timeScale = 1;
        }

        /// <summary>
        /// Sets the timescale to 0, thereby pausing anything affected by time.
        /// </summary>
        /// <param name="multiplier">Multiplier.</param>
        /// <param name="message">Message.</param>
        void OnGameWin(int multiplier, string message)
        {
            Time.timeScale = 0;
        }

        /// <summary>
        /// Initializes PlayerManager, WaveSystem and GameCondtions objects, by filling up their dependencies.
        /// </summary>
        /// <param name="playercount">Playercount.</param>
        void OnStartGame(int playercount)
        {
            playerManager.Initialize(playercount, bulletManager, gameConfig.playerAttributes, playArea);
            waveSystem.Initalize(gameConfig.waves, bulletManager, playArea, lifeZone);
            gameConditions.Initialize(gameConfig.winConditions);
        }

        void Update()
        {
            playerManager.OnUpdate();
            waveSystem.OnUpdate();
        }

        void FixedUpdate()
        {
            bulletManager.OnFixedUpdate();
            playerManager.OnFixedUpdate();
            waveSystem.OnFixedUpdate();
        }

        void OnPlayAgain()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }
}