﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SpaceShooter
{
    /// <summary>
    /// Game events.
    /// </summary>
    public class GameEvents
    {
        #region HUD events
        public const string SetupPlayerHUD = "SetupPlayerHUD";
        public const string UpdateScore = "UpdateScore";
        public const string UpdateLives = "UpdateLives";
        public const string ShowHighestScore = "ShowHighestScore";
        #endregion

        #region Score update events
        public const string NewPlayerCreated = "NewPlayerCreated";
        public const string EnemyKilled = "EnemyKilled";
        #endregion

        #region Game over events
        public const string PlayerGameOver = "PlayerGameOver";
        public const string GameOver = "GameOver";
        #endregion

        #region UI events
        public const string ShowHighScores = "ShowHighScores";
        public const string Menu = "Menu";
        public const string PanelOpen = "PanelOpen";
        #endregion

        public const string PlayAgain = "PlayAgain";
        public const string StartGame = "StartGame";
        public const string GameWin = "GameWin";
        public const string ResumeGame = "ResumeGame";
    }
}

