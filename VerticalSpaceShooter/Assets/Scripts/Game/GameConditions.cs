﻿using System.Collections;
using UnityEngine;

namespace SpaceShooter
{
    /// <summary>
    /// Game conditions.
    /// The class which checks for game win conditions and triggers them.
    /// </summary>
    public class GameConditions : MonoBehaviour
    {
        float gameTimer;
        bool isGameOver;

        void OnEnable()
        {
            Messenger.AddListener(GameEvents.GameOver, OnGameOver);
        }

        void OnDisable()
        {
            Messenger.RemoveListener(GameEvents.GameOver, OnGameOver);
        }

        void OnGameOver()
        {
            isGameOver = true;
        }

        public void Initialize(WinConditions winConditions)
        {
            StartCoroutine(CheckWinCondition(winConditions));
        }

        IEnumerator CheckWinCondition(WinConditions winConditions)
        {
            while (!isGameOver)
            {
                gameTimer += Time.deltaTime;
                if (gameTimer >= winConditions.survivalTime)
                {    
                    string victoryMessage = string.Format("Well done! You have survived for {0} seconds and will be awarded {1}x score boost", 
                                                winConditions.survivalTime, 
                                                winConditions.winScoreMultiplier);
                    Messenger<int, string>.Invoke(GameEvents.GameWin, winConditions.winScoreMultiplier, victoryMessage);
                    yield break;
                }
                yield return null;
            }
        }
    }
}

