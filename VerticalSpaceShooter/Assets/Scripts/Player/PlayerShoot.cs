﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SpaceShooter
{
    /// <summary>
    /// Player shoot.
    /// </summary>
    public class PlayerShoot
    {
        Controller controller;
        BulletManager bulletManager;
        string bulletLayerName;
        float spawnTimer;

        public PlayerShoot(Controller controller, BulletManager bulletManager, string bulletLayerName)
        {
            this.controller = controller;
            this.bulletManager = bulletManager;
            this.bulletLayerName = bulletLayerName;
        }

        /// <summary>
        /// Checks if the player is shooting.
        /// Spawns bullets based on the current bullet pattern if the player is indeed shooting at particular intervals.
        /// </summary>
        /// <param name="bulletPattern">Bullet pattern.</param>
        /// <param name="info">Info.</param>
        public void Shoot(BulletPattern bulletPattern, BulletInfo info)
        {
            if(!controller.Result.IsShooting())
            {
                spawnTimer = 0;
                return;
            }

            spawnTimer += Time.deltaTime;
            if(spawnTimer>bulletPattern.interval)
            {
                bulletManager.SpawnBullets(bulletPattern, info, bulletLayerName);
                spawnTimer = 0;
            }
        }
    }
}