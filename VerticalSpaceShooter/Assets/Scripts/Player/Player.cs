﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SpaceShooter
{
    /// <summary>
    /// Player.
    /// Responsible for initializing and updating the player.
    /// </summary>
    [RequireComponent(typeof(Rigidbody2D), typeof(BoxCollider2D))]
    public class Player : MonoBehaviour
    {
        public Controller controller;
        public Healthbar healthbar;
        public string bulletLayer;
        public int PlayerId{ get; private set; }
        PlayerAttributes playerAttributes;
        PlayerMovement playerMovement;
        PlayerShoot playerShoot;
        Rigidbody2D rigidbody;
        BulletManager bulletManager;
        BulletPatterns bulletPatterns;
        BulletPattern currentBulletPattern;
        int health;
        int lives;

        /// <summary>
        /// Initializes the player entity.
        /// Creates a new object for player movement functionalities.
        /// Creates a new object for player shooting functionalities.
        /// Sets the health and lives from player attributes.
        /// Sets a bullet pattern for the player.
        /// </summary>
        /// <param name="bulletManager">Bullet manager.</param>
        /// <param name="bulletPatterns">Bullet patterns.</param>
        /// <param name="playerAttributes">Player attributes.</param>
        /// <param name="playArea">Play area.</param>
        /// <param name="playerId">Player identifier.</param>
        public void Initialize(BulletManager bulletManager, BulletPatterns bulletPatterns, PlayerAttributes playerAttributes, BoxCollider2D playArea, int playerId)
        {
            this.bulletManager = bulletManager;
            this.bulletPatterns = bulletPatterns;
            this.playerAttributes = playerAttributes;
            PlayerId = playerId;

            playerMovement = new PlayerMovement(controller, playerAttributes.speed, playArea.bounds.min, playArea.bounds.max);
            playerShoot = new PlayerShoot(controller,bulletManager,bulletLayer);

            health = playerAttributes.health;
            lives = playerAttributes.lives;

            Messenger<int>.Invoke(GameEvents.NewPlayerCreated, playerId);
            Messenger<int,int>.Invoke(GameEvents.UpdateLives, playerId, playerAttributes.lives);

            OnChangeBulletPattern(0);
        }

        void Awake()
        {
            rigidbody = GetComponent<Rigidbody2D>();
        }

        /// <summary>
        /// Changes the bullet pattern to a new pattern.
        /// </summary>
        /// <param name="id">Bullet pattern identifier.</param>
        void OnChangeBulletPattern(int id)
        {            
            currentBulletPattern = bulletPatterns.patterns.Find(x => x.id == id);
        }

        /// <summary>
        /// Checks if the player is shooting.
        /// Spawns bullets based on the current bullet pattern if the player is indeed shooting.
        /// </summary>
        public void OnUpdate()
        {
            playerShoot.Shoot(currentBulletPattern, new BulletInfo(transform.position,playerAttributes.fireSpeed,playerAttributes.baseDamage,PlayerId));
        }

        /// <summary>
        /// Moves the rigidbody component of the player based on the data received by playerMovement.
        /// </summary>
        public void OnFixedUpdate()
        {
            rigidbody.MovePosition(playerMovement.Translation(transform.position));
        }

        /// <summary>
        /// On collision with other pooled objects(enemies/bullets) this event gets fired.
        /// Player's health is reduced based on the damage dealt by the pooled object.
        /// If health is less than or equal to zero, one life decreases, and if there are more lives health gets reset.
        /// If lives are equal to zero, then it's game over for this player.
        /// </summary>
        /// <param name="col">Collider.</param>
        void OnTriggerEnter2D(Collider2D col)
        {
            PooledObject pooledObject = col.GetComponent<PooledObject>();
            health -= pooledObject.GetCollisionDamage();
            pooledObject.OnKilled(PlayerId);
            if (health <= 0)
            {                
                lives--;
                if(IsGameOver())
                {
                    Dispose();
                    return;
                }
                health = playerAttributes.health;
                Messenger<int,int>.Invoke(GameEvents.UpdateLives, PlayerId, lives);
            }     

            healthbar.UpdateHealth(health, playerAttributes.health);
        }

        bool IsGameOver()
        {
            if(lives == 0)
            {
                Messenger<int>.Invoke(GameEvents.PlayerGameOver, PlayerId);
                return true;
            }

            return false;
        }

        void Dispose()
        {
            gameObject.SetActive(false);
        }
    }
}