﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SpaceShooter
{
    /// <summary>
    /// Player manager.
    /// Manages creation and updation of players.
    /// </summary>
    public class PlayerManager : MonoBehaviour
    {
        public PlayerContainer playerContainer;
        public BulletPatterns bulletPatterns;
        public float spawnHeight;
        List<Player> players;
        int playerCount;
        int gameOverCount;
        readonly float xOffset = 5;

        void OnEnable()
        {
            Messenger<int>.AddListener(GameEvents.PlayerGameOver, OnPlayerGameOver);
        }

        void OnDisable()
        {
            Messenger<int>.RemoveListener(GameEvents.PlayerGameOver, OnPlayerGameOver);
        }

        /// <summary>
        /// Creates number of players based on playerCount.
        /// If playerCount is greater than the number of player prefabs in the playerContainer, sets the playerCount to the number of prefabs.
        /// Passes relevant data to the players such as playerAttributes and playArea.
        /// Gives the players access to bulletManager.
        /// </summary>
        /// <param name="numPlayers">Number of players.</param>
        /// <param name="bulletManager">Bullet manager.</param>
        /// <param name="playerAttributes">Player attributes.</param>
        /// <param name="playArea">Play area.</param>
        public void Initialize(int numPlayers, BulletManager bulletManager, PlayerAttributes playerAttributes, BoxCollider2D playArea)
        {
            playerCount = numPlayers;
            if (playerCount > playerContainer.players.Count)
            {
                playerCount = playerContainer.players.Count;
                Debug.Log("Need more player data");
            }

            StartCoroutine(CreatePlayers(bulletManager, playerAttributes, playArea));
        }

        /// <summary>
        /// Creates the players.
        /// Waits for the end of frame to create the players, so that the other dependant systems are initialized first.
        /// </summary>
        /// <param name="bulletManager">Bullet manager.</param>
        /// <param name="playerAttributes">Player attributes.</param>
        /// <param name="playArea">Play area.</param>
        IEnumerator CreatePlayers(BulletManager bulletManager, PlayerAttributes playerAttributes, BoxCollider2D playArea)
        {
            yield return new WaitForEndOfFrame();

            spawnHeight = Mathf.Clamp(spawnHeight, playArea.bounds.min.y, playArea.bounds.max.y);
            float xPos = (playerCount - 1) * xOffset / 2 * -1;
            players = new List<Player>();
            for (int i = 0; i < playerCount; i++)
            {
                Vector2 position = new Vector2(xPos, spawnHeight);
                Messenger<int,PlayerHUD>.Invoke(GameEvents.SetupPlayerHUD, playerContainer.players[i].id, playerContainer.players[i].hudPrefab);
                Player player = Instantiate(playerContainer.players[i].prefab, position, Quaternion.identity, transform);
                player.Initialize(bulletManager, bulletPatterns, playerAttributes, playArea,playerContainer.players[i].id);
                players.Add(player);
                xPos += xOffset;
            }
        }

        public void OnUpdate()
        {
            if(players == null)
            {
                return;
            }

            foreach (var p in players)
            {
                if(p.gameObject.activeSelf)
                {
                    p.OnUpdate();
                }              
            }
        }

        public void OnFixedUpdate()
        {
            if(players == null)
            {
                return;
            }

            foreach (var p in players)
            {
                if(p.gameObject.activeSelf)
                {
                    p.OnFixedUpdate();
                }
            }
        }

        /// <summary>
        /// If it's a game over for all the players, triggers the game over event.
        /// </summary>
        /// <param name="playerId">Player identifier.</param>
        void OnPlayerGameOver(int playerId)
        {
            gameOverCount++;
            if(gameOverCount == playerCount)
            {
                Messenger.Invoke(GameEvents.GameOver);
            }
        }
    }
}
