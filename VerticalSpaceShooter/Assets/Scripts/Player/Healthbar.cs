﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace SpaceShooter
{
    /// <summary>
    /// Healthbar.
    /// The health bar component of the player.
    /// Blinks when the health is below a certain percentage.
    /// </summary>
    public class Healthbar : MonoBehaviour
    {
        public Image healthBarFG;
        float blinkRatio = 0.4f;
        bool isBlinking;

        public void UpdateHealth(float health, float maxHealth)
        {
            float ratio = health / maxHealth;
            healthBarFG.fillAmount = ratio;

            if(ratio<blinkRatio)
            {
                if(!isBlinking) ToggleBlink(true);
                return;
            }

            ToggleBlink(false);
        }

        public void ToggleBlink(bool canBlink)
        {
            if(canBlink)
            {
                StartCoroutine(BlinkEffect());
                return;
            }

            StopAllCoroutines();
            healthBarFG.enabled = true;
            isBlinking = false;
        }
            
        IEnumerator BlinkEffect()
        {
            isBlinking = true;
            while(true)
            {
                yield return new WaitForSeconds(0.1f);
                healthBarFG.enabled = false;
                yield return new WaitForSeconds(0.1f);
                healthBarFG.enabled = true;
            }
        }
    }
}

