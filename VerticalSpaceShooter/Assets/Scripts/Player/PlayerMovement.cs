﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SpaceShooter
{
    /// <summary>
    /// Player movement.
    /// </summary>
    public class PlayerMovement
    {
        Controller controller;
        float speed;
        Vector2 min;
        Vector2 max;

        public PlayerMovement(Controller controller, float speed, Vector2 min, Vector2 max)
        {
            this.controller = controller;
            this.speed = speed;
            this.min = min;
            this.max = max;
        }
            
        /// <summary>
        /// Gets the new position based on the input, currentPosition and speed.
        /// </summary>
        /// <param name="currentPosition">Current position.</param>
        public Vector2 Translation(Vector2 currentPosition)
        {
            float xPos = currentPosition.x + controller.Result.HorizontalMovement() * Time.deltaTime * speed;
            float yPos = currentPosition.y + controller.Result.VerticalMovement() * Time.deltaTime * speed;
            xPos = Mathf.Clamp(xPos, min.x, max.x);
            yPos = Mathf.Clamp(yPos, min.y, max.y);
            return new Vector2(xPos, yPos);
        }
    }
}

