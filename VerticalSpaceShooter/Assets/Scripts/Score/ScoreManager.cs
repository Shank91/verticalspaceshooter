﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

namespace SpaceShooter
{
    /// <summary>
    /// Score manager.
    /// Responsible for updating player scores and saving highscores.
    /// </summary>
    public class ScoreManager : MonoBehaviour
    {
        Dictionary<int,int> playerScores;
        int maxHighScores = 5;
        const string HighScoreSave = "HighScoreSave";

        void OnEnable()
        {
            Messenger<int,string>.AddListener(GameEvents.GameWin, OnGameWin);
            Messenger<int>.AddListener(GameEvents.NewPlayerCreated, OnNewPlayerCreated);
            Messenger<int,int>.AddListener(GameEvents.EnemyKilled, OnEnemyKilled);
            Messenger.AddListener(GameEvents.GameOver, OnGameOver);
        }

        void OnDisable()
        {
            Messenger<int, string>.RemoveListener(GameEvents.GameWin, OnGameWin);
            Messenger<int>.RemoveListener(GameEvents.NewPlayerCreated, OnNewPlayerCreated);
            Messenger<int,int>.RemoveListener(GameEvents.EnemyKilled, OnEnemyKilled);
            Messenger.RemoveListener(GameEvents.GameOver, OnGameOver);
        }

        /// <summary>
        /// On game win, multiplies the score.
        /// </summary>
        /// <param name="multiplier">Multiplier.</param>
        /// <param name="message">Message.</param>
        void OnGameWin(int multiplier, string message)
        {
            playerScores = playerScores.ToDictionary(x => x.Key, x => x.Value * multiplier);

            foreach (var s in playerScores)
            {
                Messenger<int, int>.Invoke(GameEvents.UpdateScore, s.Key, s.Value);
            }
        }

        void Start()
        {
            playerScores = new Dictionary<int, int>();
        }

        /// <summary>
        /// Adds a new player score for the playerId.
        /// </summary>
        /// <param name="playerId">Player identifier.</param>
        void OnNewPlayerCreated(int playerId)
        {
            playerScores.Add(playerId, 0);
            Messenger<int,int>.Invoke(GameEvents.UpdateScore, playerId, playerScores[playerId]);
            Messenger<int>.Invoke(GameEvents.ShowHighestScore, GetHighestScore());
        }

        /// <summary>
        /// Increments the score when an enemy is killed.
        /// </summary>
        /// <param name="playerId">Player identifier.</param>
        /// <param name="scoreIncrement">Score increment.</param>
        void OnEnemyKilled(int playerId, int scoreIncrement)
        {
            playerScores[playerId] += scoreIncrement;
            Messenger<int,int>.Invoke(GameEvents.UpdateScore, playerId, playerScores[playerId]);
        }

        /// <summary>
        /// Save high scores on game over.
        /// </summary>
        void OnGameOver()
        {
            SaveHighScores();
        }

        /// <summary>
        /// Gets the highest score among all the high scores.
        /// </summary>
        /// <returns>The highest score.</returns>
        int GetHighestScore()
        {
            string json = PlayerPrefs.GetString(HighScoreSave);
            HighScores highScores = JsonUtility.FromJson<HighScores>(json);
            if(highScores == null||highScores.scores == null||highScores.scores.Count==0)
            {
                return 0;
            }

            return highScores.scores.Max();
        }

        /// <summary>
        /// Saves the high scores.
        /// The number of high scores will not be greater than maxHighScores.
        /// </summary>
        void SaveHighScores()
        {
            string json = PlayerPrefs.GetString(HighScoreSave);
            HighScores highScores = JsonUtility.FromJson<HighScores>(json);
            if(highScores == null)
            {
                highScores = new HighScores();
                highScores.scores = new List<int>();
            }

            foreach(var key in playerScores.Keys)
            {
                if(highScores.scores.Count<maxHighScores)
                {
                    highScores.scores.Add(playerScores[key]);
                    continue;
                }

                int lowestScore = highScores.scores.Min();
                if(playerScores[key]>lowestScore)
                {
                    highScores.scores.Remove(lowestScore);
                    highScores.scores.Add(playerScores[key]);
                }
            }

            json = JsonUtility.ToJson(highScores);
            PlayerPrefs.SetString(HighScoreSave, json);
        }
    }

    [System.Serializable]
    public class HighScores
    {
        public List<int> scores;
    }
}

