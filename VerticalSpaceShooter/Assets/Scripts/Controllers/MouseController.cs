﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SpaceShooter
{
    /// <summary>
    /// Mouse controller.
    /// </summary>
    public class MouseController : MonoBehaviour, IController
    {
        #region IController implementation

        public bool IsShooting()
        {
            return Input.GetKey(KeyCode.Mouse0);
        }

        public float HorizontalMovement()
        {
            return Input.GetAxis("Mouse X");
        }

        public float VerticalMovement()
        {
            return Input.GetAxis("Mouse Y");
        }

        #endregion


    }
}
