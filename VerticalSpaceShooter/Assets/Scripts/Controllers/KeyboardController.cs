﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SpaceShooter
{
    /// <summary>
    /// Keyboard controller.
    /// </summary>
    public class KeyboardController : MonoBehaviour, IController
    {
        #region IController implementation

        public bool IsShooting()
        {
            return Input.GetKey(KeyCode.Z);
        }

        public float HorizontalMovement()
        {
            return Input.GetAxis("Horizontal");
        }

        public float VerticalMovement()
        {
            return Input.GetAxis("Vertical");
        }

        #endregion
    }

}
