﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SpaceShooter
{
    /// <summary>
    /// Bullet generator.
    /// Responsible for pooling bullet entities.
    /// </summary>
    public class BulletGenerator : PooledObjectGenerator<Bullet>
    {
        
    }

}
