﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SpaceShooter
{
    /// <summary>
    /// Bullet data.
    /// Modifyable vertical and horizontal base speeds.
    /// </summary>
    [System.Serializable]
    public class BulletData : IPooledObjectData
    {
        [Range(-1,1)]
        public float verticalSpeed, horizontalSpeed;
    }

    /// <summary>
    /// Bullet info.
    /// Stores additional information which are position, speed, damage and optional sourceId.
    /// </summary>
    public class BulletInfo : ObjectInfo
    {
        public BulletInfo(Vector2 position, float speed, int damage, int sourceId = -1) : base(position,speed,damage,sourceId)
        {
            
        }
    }
}