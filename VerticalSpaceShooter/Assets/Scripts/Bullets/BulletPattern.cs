﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SpaceShooter
{
    /// <summary>
    /// Bullet pattern.
    /// Modifyable data container.
    /// Sets an id for the pattern, which should be unique.
    /// Sets a sprite.
    /// Has option for creating multiple bullet data, needs atleast one to function.
    /// Sets the interval for spawning bullets. 
    /// </summary>
    [System.Serializable]
    public class BulletPattern
    {
        public int id;
        public Sprite sprite;
        public List<BulletData> bulletData;
        [Range(0.1f,5f)]
        public float interval;
    }
}