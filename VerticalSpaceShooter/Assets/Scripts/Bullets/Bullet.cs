﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace SpaceShooter
{
    /// <summary>
    /// Bullet.
    /// Inherits from PooledObject as it also gets pooled.
    /// Initializes and updates a single bullet entity
    /// </summary>
    public class Bullet : PooledObject
    {
        BulletData bulletData;

        #region implemented abstract members of PooledObject

        /// <summary>
        /// Extracts bullet data.
        /// </summary>
        /// <param name="data">Data.</param>
        protected override void ExtractData(IPooledObjectData data)
        {
            bulletData = (BulletData)data;
        }

        protected override void InitializeTransform()
        {
            transform.position = Info.Position;
            transform.up = new Vector2(bulletData.horizontalSpeed,bulletData.verticalSpeed);
        }

        /// <summary>
        /// Gets the distance delta for a frame based on bullet info speed and bullet data horizontal and vertical base speeds.
        /// </summary>
        /// <returns>The distance delta.</returns>
        protected override Vector2 GetDistanceDelta()
        {
            return new Vector2(bulletData.horizontalSpeed * Info.Speed, bulletData.verticalSpeed * Info.Speed);
        }

        #endregion
    }
}