﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SpaceShooter
{
    /// <summary>
    /// Bullet manager.
    /// Has methods for spawning bullets.
    /// Also has methods for updating active bullets.
    /// </summary>
    public class BulletManager : MonoBehaviour
    {
        public BulletGenerator bulletGenerator;
        BoxCollider2D lifeZone;
        List<Bullet> activeBullets;

        public void Initialize(BoxCollider2D lifeZone)
        {
            this.lifeZone = lifeZone;
            activeBullets = new List<Bullet>();
        }
            
        public void OnFixedUpdate()
        {
            UpdateBullets();
        }

        /// <summary>
        /// Updates the active bullets.
        /// </summary>
        void UpdateBullets()
        {
            if(activeBullets == null)
            {
                return;
            }

            for(int i=0; i<activeBullets.Count; i++)
            {
                activeBullets[i].OnUpdate();
            }
        }

        /// <summary>
        /// Spawns bullets based on the passed bullet pattern and bullet info.
        /// Adds the spawned bullet to the list of active bullets.
        /// Sets the bullet layer, for proper layer to layer collision detection.
        /// </summary>
        /// <param name="pattern">Pattern.</param>
        /// <param name="info">Info.</param>
        /// <param name="layerName">Layer name.</param>
        public void SpawnBullets(BulletPattern pattern, BulletInfo info, string layerName)
        {
            foreach(var b in pattern.bulletData)
            {
                Bullet bullet = bulletGenerator.GetElementFromPool();
                bullet.gameObject.layer = LayerMask.NameToLayer(layerName);
                bullet.Initialize(b, pattern.sprite,info,lifeZone.bounds,()=>{
                    activeBullets.Remove(bullet);
                });
                activeBullets.Add(bullet);
            }
        }
    }
}
