﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SpaceShooter
{
    /// <summary>
    /// Background.
    /// Updates the background texture offset.
    /// </summary>
    public class Background : MonoBehaviour
    {
        public float speed = 0.01f;
        private Material material;

        void Start()
        {
            material = GetComponent<Renderer>().material;
        }

        void Update()
        {
            material.mainTextureOffset = new Vector2(0, Time.time * speed);
        }
    }
}

