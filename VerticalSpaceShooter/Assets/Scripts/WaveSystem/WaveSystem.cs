﻿using UnityEngine;

namespace SpaceShooter
{
    /// <summary>
    /// Wave system.
    /// Responsible for initializing the enemy system.
    /// The creation of enemies are dependant on waves.
    /// Each wave will create a certain number and type of enemies and will last for a certain time.
    /// </summary>
    public class WaveSystem : MonoBehaviour
    {
        public EnemyManager enemyManager;
        readonly BulletManager bulletManager;
        BoxCollider2D playArea;
        BoxCollider2D lifeZone;
        Vector2 minBounds;
        Vector2 maxBounds;

        public void Initalize(Waves waves, BulletManager bulletManager, BoxCollider2D playArea, BoxCollider2D lifeZone)
        {
            this.playArea = playArea;
            this.lifeZone = lifeZone;
            minBounds = Camera.main.WorldToViewportPoint(playArea.bounds.min);
            maxBounds = Camera.main.WorldToViewportPoint(playArea.bounds.max);
            enemyManager.Initialize(waves.enemyWaves, bulletManager, lifeZone, minBounds, maxBounds);
        }

        public void OnUpdate()
        {
            enemyManager.OnUpdate();
        }

        public void OnFixedUpdate()
        {
            enemyManager.OnFixedUpdate();
        }
    }
}
