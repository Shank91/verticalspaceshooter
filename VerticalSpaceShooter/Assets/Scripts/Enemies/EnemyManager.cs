﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace SpaceShooter
{
    /// <summary>
    /// Enemy manager.
    /// Has methods for spawning enemies.
    /// Also has methods for updating active enemies.
    /// </summary>
    public class EnemyManager : MonoBehaviour
    {
        public EnemyGenerator enemyGenerator;
        public EnemyContainer enemyContainer;
        public BulletPatterns bulletPatterns;
        BulletManager bulletManager;
        BoxCollider2D lifeZone;
        Vector2 minBounds;
        Vector2 maxBounds;
        List<Enemy> activeEnemies;
        List<Wave> enemyWaves;
        int currentWave;
        float currentTime;
        float spawnYPos = 1.2f;

        public void Initialize(List<Wave> enemyWaves, BulletManager bulletManager, BoxCollider2D lifeZone, Vector2 minBounds, Vector2 maxBounds)
        {
            activeEnemies = new List<Enemy>();
            this.enemyWaves = enemyWaves;
            this.bulletManager = bulletManager;
            this.lifeZone = lifeZone;
            this.minBounds = minBounds;
            this.maxBounds = maxBounds;
        }

        public void OnUpdate()
        {
            currentTime += Time.deltaTime;
            if(enemyWaves == null||enemyWaves.Count == 0)
            {
                return;
            }

            if(currentTime>=enemyWaves[currentWave].time)
            {
                StartCoroutine(SpawnEnemies(enemyWaves[currentWave]));
                currentWave = currentWave + 1 < enemyWaves.Count ? currentWave + 1 : 0;
                if(currentWave == 0)  currentTime = 0;
            }
        }

        public void OnFixedUpdate()
        {
            UpdateEnemies();
        }

        /// <summary>
        /// Updates the active enemies.
        /// </summary>
        void UpdateEnemies()
        {
            if(activeEnemies == null)
            {
                return;
            }

            for(int i=0; i<activeEnemies.Count; i++)
            {
                activeEnemies[i].OnUpdate();
            }
        }

        /// <summary>
        /// Spawns the enemies based on the enemy wave.
        /// If the enemyWave has shooting type enemies then gets shooting enemy patterns from the enemycontainer, otherwise gets normal enemy patterns
        /// Waits for a certain interval before spawning each enemy, except the first one.
        /// </summary>
        /// <param name="enemywave">Enemywave.</param>
        IEnumerator SpawnEnemies(Wave enemywave)
        {
            IEnumerable<IEnemyPattern> enemies = enemyContainer.normalEnemies;
            if(enemywave.enemyType == EnemyTypes.Shooting)
            {
                enemies = enemyContainer.shootingEnemies;
            }

            for(int i=0;i<enemywave.count;i++)
            {
                SpawnEnemy(enemies, enemywave);
                yield return new WaitForSeconds(enemywave.spawnInterval);
            }
        }

        /// <summary>
        /// Spawns one enemy based on the pattern and the enemywave data.
        /// If it's a shooting enemy type, initializes the ShootingEnemy component of the enemy with bulletPattern and firespeed.
        /// Passes a random x position for the enemy to spawn at within the minimum and maximum bounds, whereas the y position is fixed.
        /// Adds the spawned enemy to a list of active enemies.
        /// </summary>
        /// <param name="enemies">Enemies.</param>
        /// <param name="enemyWave">Enemy wave.</param>
        void SpawnEnemy(IEnumerable<IEnemyPattern> enemies, Wave enemyWave)
        {
            Enemy enemy = enemyGenerator.GetElementFromPool();
            IEnemyPattern enemyPattern = enemies.Where(x => x.Id == enemyWave.enemyId).Single();
            enemy.CanShoot = (enemyWave.enemyType == EnemyTypes.Shooting);
            if(enemyWave.enemyType == EnemyTypes.Shooting)
            {
                ShootingEnemyPattern shootingEnemyPattern = (ShootingEnemyPattern)enemyPattern;
                enemy.shootingEnemy.Initialize(bulletManager, bulletPatterns.patterns.Find(x => x.id == shootingEnemyPattern.bulletPatternId), shootingEnemyPattern.fireSpeed);
            }

            EnemyInfo info = new EnemyInfo(new Vector2(Random.Range(minBounds.x, maxBounds.x), spawnYPos), enemyWave.speed, enemyWave.damage);
            enemy.Initialize(enemyPattern.Data, enemyPattern.Sprite, info, lifeZone.bounds,()=>{
                activeEnemies.Remove(enemy);
            });
            activeEnemies.Add(enemy);
        }
    }
}
