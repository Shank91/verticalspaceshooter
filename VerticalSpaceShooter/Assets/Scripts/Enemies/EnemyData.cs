﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SpaceShooter
{
    /// <summary>
    /// Enemy pattern.
    /// Modifyable data container.
    /// Sets an id for the pattern, which should be unique.
    /// Sets a sprite.
    /// Creates new enemydata.
    /// </summary>
    [System.Serializable]
    public class EnemyPattern : IEnemyPattern
    {
        public int id;
        public Sprite sprite;
        public EnemyData enemyData;

        #region IEnemy implementation

        public int Id { get { return id; }}

        public Sprite Sprite { get { return sprite;}}

        public EnemyData Data { get { return enemyData;}}


        #endregion
    }

    /// <summary>
    /// Shooting enemy pattern.
    /// Derives from EnemyPattern.
    /// Has additional info required for shooting such as the bulletPatternId and speed of firing bullets.
    /// </summary>
    [System.Serializable]
    public class ShootingEnemyPattern : EnemyPattern
    {
        public int bulletPatternId;
        [Range(1,100)]
        public float fireSpeed;
    }

    /// <summary>
    /// Enemy data.
    /// Modifyable vertical and horizontal base speeds.
    /// Modifyable animation curves for x and y axis movements.
    /// Modifyable health.
    /// </summary>
    [System.Serializable]
    public class EnemyData : IPooledObjectData
    {
        [Range(-1, 1)]
        public float verticalSpeed, horizontalSpeed;
        public AnimationCurve xCurve;
        public AnimationCurve yCurve;
        [Range(1,100)]
        public int health;
    }

    /// <summary>
    /// Enemy info.
    /// Stores additional information which are position, speed, damage and optional sourceId.
    /// </summary>
    public class EnemyInfo : ObjectInfo
    {
        public EnemyInfo(Vector2 position, float speed, int damage, int sourceId = -1) : base(position,speed,damage,sourceId)
        {

        }
    }
        
    public enum EnemyTypes
    {
        Normal = 0,
        Shooting = 1,
        Unassigned = 2
    }
}
