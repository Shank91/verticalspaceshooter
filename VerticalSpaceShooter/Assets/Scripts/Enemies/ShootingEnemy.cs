﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SpaceShooter
{
    /// <summary>
    /// Shooting enemy.
    /// The shooting component on an enemy entity.
    /// Responsible for spawning bullets based on the received bullet pattern.
    /// </summary>
    public class ShootingEnemy : MonoBehaviour
    {
        public string bulletLayer;
        BulletManager bulletManager;
        PlayerShoot enemyShoot;
        BulletPattern bulletPattern;
        float fireSpeed;
        float spawnTimer;

        public void Initialize(BulletManager bulletManager, BulletPattern bulletPattern, float fireSpeed)
        {
            this.bulletManager = bulletManager;
            this.bulletPattern = bulletPattern;
            this.fireSpeed = fireSpeed;
        }

        /// <summary>
        /// Spawns bullets after a certain interval.
        /// </summary>
        /// <param name="damage">Damage.</param>
        public void OnUpdate(int damage)
        {
            spawnTimer += Time.deltaTime;
            if(spawnTimer>bulletPattern.interval)
            {
                BulletInfo info = new BulletInfo(transform.position,fireSpeed,damage);
                bulletManager.SpawnBullets(bulletPattern, info, bulletLayer);
                spawnTimer = 0;
            }
        }
    }
}

