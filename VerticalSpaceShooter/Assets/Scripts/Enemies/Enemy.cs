﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SpaceShooter
{
    /// <summary>
    /// Enemy class.
    /// Inherits from PooledObject as it also gets pooled.
    /// Initializes and updates a single enemy entity.
    /// Has a ShootingEnemy component which can be used if the enemy can shoot.
    /// </summary>
    public class Enemy : PooledObject
    {
        public ShootingEnemy shootingEnemy;
        public bool CanShoot{ get; set; }
        EnemyData enemyData;
        int health;

        #region implemented abstract members of PooledObject
        /// <summary>
        /// Extracts enemy data.
        /// Sets enemy health based on data.
        /// </summary>
        /// <param name="data">Data.</param>
        protected override void ExtractData(IPooledObjectData data)
        {
            enemyData = (EnemyData)data;
            health = enemyData.health;
        }

        protected override void InitializeTransform()
        {
            transform.position = (Vector2)Camera.main.ViewportToWorldPoint(Info.Position);
            transform.up = new Vector2(enemyData.horizontalSpeed,enemyData.verticalSpeed);
        }

        /// <summary>
        /// Gets the distance delta on x axis based on the horizonal speed data, info speed and an animation curve.
        /// Gets the distance delta on y axis based on the vertical speed data, info speed and an animation curve.
        /// </summary>
        /// <returns>The distance delta.</returns>
        protected override Vector2 GetDistanceDelta()
        {
            float xDelta = enemyData.horizontalSpeed * Info.Speed * enemyData.xCurve.Evaluate(Time.time);
            float yDelta = enemyData.verticalSpeed * Info.Speed * enemyData.yCurve.Evaluate(Time.time);
            return new Vector2(xDelta, yDelta);
        }

        #endregion

        /// <summary>
        /// Updates the entity's position.
        /// If the entity goes out of playable bounds, disposes off the entity.
        /// Also updates the shooting enemy component if the entity can shoot.
        /// </summary>
        public override void OnUpdate()
        {
            base.OnUpdate();
            if (CanShoot)
            {
                shootingEnemy.OnUpdate(Info.Damage);
            }
        }

        /// <summary>
        /// Returns the damage on direct collision with this entity.
        /// Doubles the damage on direct collision, if it's a shooting enemy.
        /// </summary>
        /// <returns>The collision damage.</returns>
        public override int GetCollisionDamage()
        {
            return CanShoot ? Info.Damage : Info.Damage * 2;
        }

        void OnTriggerEnter2D(Collider2D col)
        {
            CheckBulletCollision(col);
        }

        /// <summary>
        /// Checks the bullet collision.
        /// If hit by another pooled entity, reduces enemy's health, and disposes off the other entity.
        /// If health is less than or equal to zero, disposes off this entity, and triggers an EnemyKilled event.
        /// </summary>
        /// <param name="col">Collider.</param>
        void CheckBulletCollision(Collider2D col)
        {
            if (col.GetComponent<PooledObject>() == null)
            {
                return;
            }

            PooledObject pooledObject = col.GetComponent<PooledObject>();
            pooledObject.Dispose();
            health -= pooledObject.GetCollisionDamage();
            if (health <= 0)
            {
                OnKilled(pooledObject.GetSourceId());
            }     
        }

        public override void OnKilled(int playerId)
        {
            Messenger<int,int>.Invoke(GameEvents.EnemyKilled, playerId, enemyData.health);
            Dispose();
        }

    }
}