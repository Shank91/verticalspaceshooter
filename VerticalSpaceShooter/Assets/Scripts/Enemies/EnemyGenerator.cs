﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SpaceShooter
{
    /// <summary>
    /// Enemy generator.
    /// Responsible for pooling enemy entities.
    /// </summary>
    public class EnemyGenerator : PooledObjectGenerator<Enemy>
    {

    }

}
