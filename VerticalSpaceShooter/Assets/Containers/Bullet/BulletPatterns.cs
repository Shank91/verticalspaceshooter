﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SpaceShooter
{
    [CreateAssetMenu(fileName = "BulletPatterns", menuName = "Vertical Space Shooter/BulletPatterns")]
    public class BulletPatterns : ScriptableObject
    {
        public List<BulletPattern> patterns;
    }
}