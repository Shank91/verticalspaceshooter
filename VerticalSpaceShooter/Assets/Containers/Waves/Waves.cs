﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SpaceShooter
{
    [CreateAssetMenu(fileName = "Waves", menuName = "Vertical Space Shooter/Waves")]
    public class Waves : ScriptableObject
    {
        public List<Wave> enemyWaves;
    }

    [System.Serializable]
    public class Wave
    {
        public EnemyTypes enemyType;
        public int enemyId;
        [Range(1,20)]
        public int count;
        public float time;
        [Range(0.1f,5f)]
        public float spawnInterval;
        [Range(1,50)]
        public int damage;
        [Range(0.1f,20f)]
        public float speed;
    }
}
