﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SpaceShooter
{
    [CreateAssetMenu(fileName = "EnemyContainer", menuName = "Vertical Space Shooter/EnemyContainer")]
    public class EnemyContainer : ScriptableObject
    {
        public List<EnemyPattern> normalEnemies;
        public List<ShootingEnemyPattern> shootingEnemies;
    }
}