﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SpaceShooter
{
    [CreateAssetMenu(fileName = "GameConfig", menuName = "Vertical Space Shooter/GameConfig")]
    public class GameConfig : ScriptableObject
    {
        public WinConditions winConditions;
        public PlayerAttributes playerAttributes;
        public Waves waves;
    }

    [System.Serializable]
    public class PlayerAttributes
    {
        [Range(1,50)]
        public float speed;
        [Range(1,50)]
        public float fireSpeed;
        [Range(1,100)]
        public int baseDamage;
        [Range(1,10)]
        public int lives;
        [Range(10,1000)]
        public int health;
    }

    [System.Serializable]
    public class WinConditions
    {
        [Range(5,1000)]
        public float survivalTime;
        [Range(2,10)]
        public int winScoreMultiplier;
    }
}

