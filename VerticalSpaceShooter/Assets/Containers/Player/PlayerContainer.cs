﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace SpaceShooter
{
    [CreateAssetMenu(fileName = "PlayerContainer", menuName = "Vertical Space Shooter/PlayerContainer")]
    public class PlayerContainer : ScriptableObject 
    {
        public List<PlayerData> players;
    }

    [System.Serializable]
    public class PlayerData
    {
        public int id;
        public Player prefab;
        public PlayerHUD hudPrefab;
    }
}
