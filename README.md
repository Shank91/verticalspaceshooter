# VerticalSpaceShooter

This is a space shooter experiment which uses AnimationCurves for enemy movement patterns

## Details

- Unity Version: 2018.2.2f1
- Platforms: PC, Mac and Linux
- Orientation: Landscape
- Startup Scene path: Assets/Scenes/SpaceShooter.unity

## How To Play

- "Movement keys" for moving the ship.
- "Z" key for firing
